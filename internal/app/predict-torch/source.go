package predict_torch

import (
	_ "github.com/sugarme/gotch"
	_ "github.com/sugarme/gotch/nn"
	ts "github.com/sugarme/gotch/tensor"
	desc "gitlab.com/martyn.andrw/predict-torch/pkg/predict-torch"
	"log"
)

type PredictDemoAPI struct {
	Model ts.CModule
	desc.UnimplementedPredictTorchServer
}

func PredictAPI() desc.PredictTorchServer {
	model, err := ts.ModuleLoad("models/TorchModel.pt")
	if err != nil {
		log.Println(err)
	}

	return &PredictDemoAPI{*model, desc.UnimplementedPredictTorchServer{}}
}
