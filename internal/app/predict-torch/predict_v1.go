package predict_torch

import (
	"context"
	"errors"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"math"
	"os"

	"github.com/nfnt/resize"
	_ "github.com/sugarme/gotch"
	_ "github.com/sugarme/gotch/nn"
	ts "github.com/sugarme/gotch/tensor"
	desc "gitlab.com/martyn.andrw/predict-torch/pkg/predict-torch"
)

func (a *PredictDemoAPI)PredictV1(ctx context.Context, req *desc.RequestV1) (*desc.ResponseV1, error) {
	model := a.Model
	if req.PathModel != "" {
		model_load, err := ts.ModuleLoad(req.PathModel)
		if err != nil{
			log.Println(errors.New("The model cannot be loaded. Check your path: " + req.PathModel))
		} else {
			model = *model_load
		}
	}

	file_opened, err := os.Open(req.PathImg)
	if err != nil {
		log.Println(err)
	} else {
		defer file_opened.Close()
	}

	img, err := jpeg.Decode(file_opened)
	if err != nil {
		log.Println(os.Stderr, "%s: %v\n", file_opened.Name(), err)
	}

	img_data := rgb_to_data(img)
	ts_img, err := to_tensor(img_data)
	if err != nil{
		log.Println(err)
	}

	ts_img, err = ts_img.Unsqueeze(0, true)
	ts_img, err = ts_img.Unsqueeze(0, true)
	predicted, err := predict(model, ts_img)
	if err != nil {
			log.Println(err)
		}

	names, procents, err := who_is_it(predicted)
	if err != nil{
		log.Println(err)
	}

	fmt.Println(names)
	fmt.Println(procents)

	return &desc.ResponseV1{Who: names,  Procents: procents}, nil
}

var (
	CAT = []float32{1, 0}
	DOG = []float32{0, 1}
	NOTHING = []float32{0, 0}
	IMG_SIZE = 50
)

func rgb_to_data(img image.Image) [][]float32{
	var img_data [][]float32

	img = resize.Resize(uint(IMG_SIZE), uint(IMG_SIZE), img, resize.Lanczos3)

	b := img.Bounds()
	imgSet := image.NewRGBA(b)
	for y := 0; y < b.Max.Y; y++ {
		var help_arr []float32
		for x := 0; x < b.Max.X; x++ {
			oldPixel := img.At(x, y)
			r, g, b, _ := oldPixel.RGBA()
			pix := 0.299*float32(r) + 0.587*float32(g) + 0.114*float32(b)
			pixel := color.Gray{uint8(pix / 256)}
			imgSet.Set(x, y, pixel)
			help_arr = append(help_arr, float32(pix/float32(math.Pow(256, 2))))
		}
		img_data = append(img_data, help_arr)
	}

	return img_data
}

func to_tensor(data [][]float32) (*ts.Tensor, error) {
	img_tensor, err := ts.NewTensorFromData(data, []int64{int64(IMG_SIZE), int64(IMG_SIZE)})
	if err != nil {
		fmt.Print("convertation error 1")
		return nil, err
	}
	return img_tensor, nil
}

func predict(model ts.CModule, ts_data *ts.Tensor) ([]float32, error){
	result := NOTHING

	predicted_class, err := model.Forward(ts_data)
	if err != nil {
		return nil, err
	}

	result = predicted_class.Vals().([]float32)
	return result, nil
}

func who_is_it(animals []float32) ([]string, []float32, error){
	size := len(animals)
	if size % 2 != 0{
		return nil, nil, errors.New("The dimension of the array []float32 must be a multiple of 2")
	}

	var names []string
	var procents []float32

	size = size / 2
	for i := 0; i < size; i++{
		buf := animals[2*i:2*(i+1)]
		procent := buf[0]
		if procent < 0.5{
			procent = 1 - procent
		}

		buf = ar_round(buf)
		switch {
		case is_equal(buf, DOG):
			names = append(names, "DOG")
		case is_equal(buf, CAT):
			names = append(names, "CAT")
		case is_equal(buf, NOTHING):
			names = append(names, "NOTHING")
		default:
			names = append(names, "CATDOG")
		}

		procents = append(procents, procent)
	}

	return names, procents, nil
}

func is_equal(lefthand []float32, righthand []float32) (bool){
	l_size := len(lefthand)
	r_size := len(righthand)
	if l_size != r_size{
		return false
	}

	for i := 0; i < l_size && i < r_size; i++{
		if lefthand[i] != righthand[i]{
			return false
		}
	}
	return true
}

func ar_round(array []float32) []float32{
	var result []float32

	for _, element := range array{
		switch {
		case element > 0.5:
			result = append(result, float32(1))
		case element < 0.5:
			result = append(result, float32(0))
		default:
			result = append(result, element)
		}
	}

	return result
}

func check_jpg(path string) bool{
	if string(path[len(path)-4 : len(path)]) == ".jpg"{
		fmt.Println("yes")
		return true
	}
	return false
}