LOCAL_BIN:=$(CURDIR)/bin

run:
	go run cmd/predict-torch/main.go

lint:
	golint ./...

test:
	go test -v ./...

.PHONY: build
build: vendor-proto .generate .build

PHONY: .generate
.generate:
		mkdir -p swagger
		mkdir -p pkg/predict-torch
		PATH=$(PATH):$(LOCAL_BIN) protoc -I vendor.protogen \
				--go_out=pkg/predict-torch --go_opt=paths=import \
				--go-grpc_out=pkg/predict-torch --go-grpc_opt=paths=import \
				--grpc-gateway_out=pkg/predict-torch \
				--grpc-gateway_opt=logtostderr=true \
				--grpc-gateway_opt=paths=import \
				--swagger_out=allow_merge=true,merge_file_name=api:swagger \
				api/pr-torch/pr-torch.proto
		mv pkg/predict-torch/gitlab.com/martyn.andrw/predict-torch/* pkg/predict-torch/
		rm -rf pkg/predict-torch/gitlab.com
		mkdir -p cmd/predict-torch

.PHONY: generate
generate: .vendor-proto .generate

.PHONY: build
build:
		go build -o $(LOCAL_BIN)/predict-torch cmd/predict-torch/main.go

.PHONY: vendor-proto
vendor-proto: .vendor-proto

.PHONY: .vendor-proto
.vendor-proto:
		mkdir -p vendor.protogen
		mkdir -p vendor.protogen/api/pr-torch
		cp api/pr-torch/pr-torch.proto vendor.protogen/api/pr-torch/pr-torch.proto
		@if [ ! -d vendor.protogen/google ]; then \
			git clone https://github.com/googleapis/googleapis vendor.protogen/googleapis &&\
			mkdir -p  vendor.protogen/google/ &&\
			mv vendor.protogen/googleapis/google/api vendor.protogen/google &&\
			rm -rf vendor.protogen/googleapis ;\
		fi


.PHONY: deps
deps: install-go-deps

.PHONY: install-go-deps
install-go-deps: .install-go-deps

.PHONY: .install-go-deps
.install-go-deps:
		ls go.mod || go mod init gitlab.com/martyn.andrw/predict-torch
		GOBIN=$(LOCAL_BIN) go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
		GOBIN=$(LOCAL_BIN) go get -u github.com/golang/protobuf/proto
		GOBIN=$(LOCAL_BIN) go get -u github.com/golang/protobuf/protoc-gen-go
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/grpc
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/protobuf/types/known/emptypb@v1.27.1
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/protobuf/reflect/protoreflect@v1.27.1
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/protobuf/runtime/protoimpl@v1.27.1
		GOBIN=$(LOCAL_BIN) go get -u github.com/nfnt/resize
		GOBIN=$(LOCAL_BIN) go get -u github.com/sugarme/gotch@v0.3.13
		GOBIN=$(LOCAL_BIN) go get -u gitlab.com/martyn.andrw/predict-torch/pkg/predict-torch
		GOBIN=$(LOCAL_BIN) go get -u gitlab.com/martyn.andrw/predict-torch/internal/app/predict-torch
		GOBIN=$(LOCAL_BIN) go get -u gopkg.in/yaml.v2
		GOBIN=$(LOCAL_BIN) go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
		GOBIN=$(LOCAL_BIN) go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger

